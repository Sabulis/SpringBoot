package com.arnas.SpringMavenMySQL.controller;

import com.arnas.SpringMavenMySQL.model.User;
import com.arnas.SpringMavenMySQL.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
public class MainController {
    @Autowired
    private UserService userService;

    @GetMapping(path = "/")
    public String welcome() {
        return "index";
    }
    @GetMapping(path = "/redirect_page")
    public String redirect() {
        return "redirect:other_page";
    }

    @GetMapping(path = "/redirect_page2")
    public String redirect2() {
        return "redirect:allusers";
    }

    @GetMapping(path= "/mysql_page")
    public String mysqlPage() {
        return "usersList";
    }

    @GetMapping(path= "/other_page")
    public String finalPage(Model info) {
        info.addAttribute("msg", "Sveiki");
        info.addAttribute("number",5);
        return "otherpage";
    }

    @RequestMapping(value={"/usersEdit","/userEdit/{id}"}, method = RequestMethod.GET)
    public String notesEditForm(Model model, @PathVariable(required = false, name = "id") Long id) {
        if (null != id) {
            model.addAttribute("user", userService.findOne(id));
        } else {
            model.addAttribute("user", new User());
        }
        return "usersEdit";
    }

    @RequestMapping(value="/allusers")
    public String notesList(Model model) {
        model.addAttribute("userList", userService.findAll());
        return "usersList";
    }

    @RequestMapping(value="/usersEdit", method = RequestMethod.POST)
    public String notesEdit(Model model, User user) {
        userService.saveUser(user);
        model.addAttribute("userList", userService.findAll());
        return "usersList";
    }

    @RequestMapping(value="/userDelete/{id}", method = RequestMethod.GET)
    public String notesDelete(Model model, @PathVariable(required = true, name = "id") Long id) {
        userService.deleteUser(id);
        model.addAttribute("userList", userService.findAll());
        return "usersList";
    }
}


