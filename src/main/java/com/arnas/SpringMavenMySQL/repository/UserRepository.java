package com.arnas.SpringMavenMySQL.repository;

import com.arnas.SpringMavenMySQL.model.User;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<User, Long> {

}

